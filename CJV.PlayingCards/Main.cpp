//Lab Exercise 2 - Playing Cards
//Chris Van Hefty

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit { Hearts, Diamonds, Spades, Clubs };

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card c)
{
	string cardsuit;
	string cardrank;
	switch (c.Suit)
	{
	case Hearts : cardsuit = "Hearts"; break;
	case Diamonds : cardsuit = "Diamonds"; break;
	case Spades: cardsuit = "Spades"; break;
	case Clubs: cardsuit = "Clubs"; break;
	}
	switch (c.Rank) 
	{
	case Two: cardrank = "Two"; break;
	case Three: cardrank = "Three"; break;
	case Four: cardrank = "Four"; break;
	case Five: cardrank = "Five"; break;
	case Six: cardrank = "Six"; break;
	case Seven: cardrank = "Seven"; break;
	case Eight: cardrank = "Eight"; break;
	case Nine: cardrank = "Nine"; break;
	case Ten: cardrank = "Ten"; break;
	case Jack: cardrank = "Jack"; break;
	case Queen: cardrank = "Queen"; break;
	case King: cardrank = "King"; break;
	case Ace: cardrank = "Ace"; break;
	}
	cout << "The " << cardrank << " Of " << cardsuit << "\n";
}

Card HighCard(Card c1, Card c2) {
	if (c1.Rank >= c2.Rank) {
		return c1;
	}
	else if (c1.Rank < c2.Rank) {
		return c2;
	}
}
int main()
{
	//Card1
	Card card1;
	card1.Rank = Three;
	card1.Suit = Diamonds;
	//Card2
	Card card2;
	card2.Rank = Jack;
	card2.Suit = Spades;
	//Card3
	Card card3;
	card3.Rank = Seven;
	card3.Suit = Clubs;

	PrintCard(card2);
	PrintCard(HighCard(card3, card1));
	
	(void)_getch();
	return 0;
}
